import { InventoryItem } from './inventory-item'
import { InventoryItemType } from './iventory-item-type';

describe('InventoryItem creation', () => {
    
    test('should throw if no item provided', () => {
        expect(() => new InventoryItem()).toThrow("No item received.")
    })

    test('should throw if name empty', () => {
        expect(() =>  new InventoryItem(
            "sword-cracked-id",
            "",
            25,
            10,
            InventoryItemType.Weapon
        )).toThrow("The item should have a name.");
    });

    test('should throw if id empty', () => {
        expect(() =>  new InventoryItem(
            "",
            "Cracked Sword",
            25,
            10,
            InventoryItemType.Weapon
        )).toThrow("The item should have an id.");
    });

    test('should create instance of InventoryItem', () => {
        const crackedSword = new InventoryItem(
            "sword-cracked-id",
            "Cracked Sword",
            25,
            10,
            InventoryItemType.Weapon
        );
        expect(crackedSword).toBeInstanceOf(InventoryItem);
    });

    test('should have id of "sword-cracked-id"', () => {
        
        const crackedSword = new InventoryItem(
            "sword-cracked-id",
            "Cracked Sword",
            25,
            10,
            InventoryItemType.Weapon
        );

        expect(crackedSword.getId()).toBe("sword-cracked-id");
    });

    test('should have name of "Cracked Sword"', () => {
        
        const crackedSword = new InventoryItem(
            "sword-cracked-id",
            "Cracked Sword",
            25,
            10,
            InventoryItemType.Weapon
        );

        expect(crackedSword.getName()).toBe("Cracked Sword");
    });
    
    test('should have weight 25', () => {
        const crackedSword = new InventoryItem(
            "sword-cracked-id",
            "Cracked Sword",
            25,
            10,
            InventoryItemType.Weapon
        );
        expect(crackedSword.getWeight()).toBe(25);
    });
    
    test('should have value of 10', () => {
        const crackedSword = new InventoryItem(
            "sword-cracked-id",
            "Cracked Sword",
            25,
            10,
            InventoryItemType.Weapon
        );
        expect(crackedSword.getValue()).toBe(10);
    });

    test('should have item type of Weapon', () => {
        const crackedSword = new InventoryItem(
            "sword-cracked-id",
            "Cracked Sword",
            25,
            10,
            InventoryItemType.Weapon
        );
        expect(crackedSword.getItemType()).toBe(InventoryItemType.Weapon);
    });
    
});