import { InventoryItemType } from "./iventory-item-type";

export class InventoryItem {
    private id: string;
    private name: string;
    private weight: number;
    private value: number;
    private itemType: InventoryItemType;

    constructor(
        id: string,
        name: string,
        weight: number,
        value: number,
        itemType: InventoryItemType
    ) {
        if (arguments.length === 0) {
            throw new Error("No item received.");
        }

        if (id === "") {
            throw new Error("The item should have an id.");
        }

        if (name === "") {
            throw new Error("The item should have a name.");
        }

        this.id = id;
        this.name = name;
        this.weight = weight;
        this.value = value;
        this.itemType = itemType;
    }

    public getId(): string {
        return this.id;
    }

    public getName(): string {
        return this.name;
    }

    public getWeight(): number {
        return this.weight;
    }

    public getValue(): number {
        return this.value;
    }

    public getItemType(): InventoryItemType {
        return this.itemType;
    }
}