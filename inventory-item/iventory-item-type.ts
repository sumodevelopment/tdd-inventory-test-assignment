export enum InventoryItemType {
    Weapon,
    Gear,
    Consumable
}