import { InventoryItem } from "../inventory-item/inventory-item";
import { InventoryItemType } from "../inventory-item/iventory-item-type";

export const MOCK_WEAPON_ITEMS = [
    new InventoryItem('q2341212', "Wooden Sword", 5, 1, InventoryItemType.Weapon),
    new InventoryItem('123123', "Cracked Sword", 20, 10, InventoryItemType.Weapon),
    new InventoryItem('123123', "Rusted Sword", 25, 15, InventoryItemType.Weapon),
    new InventoryItem('123123', "Shiny Sword", 25, 20, InventoryItemType.Weapon),
    new InventoryItem('123123', "Sharp Double Sided Sword", 30, 35, InventoryItemType.Weapon),
    new InventoryItem('123123', "Wild Spear", 20, 55, InventoryItemType.Weapon),
    new InventoryItem('123123', "Diamond Sword", 30, 80, InventoryItemType.Weapon),
    new InventoryItem('66234234', "Terrible Wooden Shield", 22, 12, InventoryItemType.Weapon),
    new InventoryItem('66234234', "Cracked Shield", 30, 22, InventoryItemType.Weapon),
    new InventoryItem('66234234', "Rusty Shield", 35, 30, InventoryItemType.Weapon),
    new InventoryItem('66234234', "Shiny Shield", 40, 40, InventoryItemType.Weapon),
    new InventoryItem('66234234', "Diamon Shield", 45, 70, InventoryItemType.Weapon),
]

export const MOCK_CONSUMABLE_ITEMS: InventoryItem[] = [
    new InventoryItem('4232344', "Small Health Potion", 10, 5, InventoryItemType.Consumable),
    new InventoryItem('5543334', "Small Power Potion", 10, 7, InventoryItemType.Consumable),
    new InventoryItem('5541134', "Puma's Scent", 10, 15, InventoryItemType.Consumable),
    new InventoryItem('5548344', "Mist Potion", 12, 40, InventoryItemType.Consumable),
    new InventoryItem('5123322', "Medium Power Potion", 15, 42, InventoryItemType.Consumable),
    new InventoryItem('5123322', "Medium Health Potion", 15, 40, InventoryItemType.Consumable),
    new InventoryItem('5123322', "Large Health Potion", 15, 60, InventoryItemType.Consumable),
    new InventoryItem('5123322', "Large Power Potion", 15, 65, InventoryItemType.Consumable),
];

export const MOCK_GEAR_ITEMS = [
    new InventoryItem('4234234', "Leather Boots", 10, 5, InventoryItemType.Gear),
    new InventoryItem('5553221', "Torn Shirt", 5, 3, InventoryItemType.Gear),
    new InventoryItem('1923475', "Cheap Trousers", 7, 2, InventoryItemType.Gear),
    new InventoryItem('1923475', "Nice Hat", 10, 15, InventoryItemType.Gear),
    new InventoryItem('1923475', "Shiny Chest Plate", 30, 40, InventoryItemType.Gear),
    new InventoryItem('1923475', "Polished Shoes", 10, 50, InventoryItemType.Gear),
    new InventoryItem('1923475', "Cotton Shirt with buttons", 10, 60, InventoryItemType.Gear),
    new InventoryItem('1923475', "Scarf of Justice", 10, 200, InventoryItemType.Gear),
]