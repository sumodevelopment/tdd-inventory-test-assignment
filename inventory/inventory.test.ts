import { InventoryItem } from '../inventory-item/inventory-item';
import { InventoryItemType } from '../inventory-item/iventory-item-type';
import Wallet from '../wallet/wallet';
import Inventory from './inventory';
import {
    MOCK_WEAPON_ITEMS,
    MOCK_CONSUMABLE_ITEMS,
    MOCK_GEAR_ITEMS
} from './inventory.mocks';

describe('Inventory slot count', () => {
    test('should have 25 slots', () => {
        const inventory: Inventory = new Inventory()
        expect(inventory.getInventorySlotCount()).toBe(25);
    })

    test('should have 30 slots', () => {
        const inventory: Inventory = new Inventory(30)
        expect(inventory.getInventorySlotCount()).toBe(30);
    })

    test('should have 100 slots', () => {
        const inventory: Inventory = new Inventory(100)
        expect(inventory.getInventorySlotCount()).toBe(100);
    })

    test('should throw if negative slot count', () => {
        
    })
})

describe('Inventory - findFirstOpenSlot()', () => {

    test('should be 0 with no items added', () => {
        const inventory: Inventory = new Inventory(25);
        expect(inventory.getFirstOpenSlot()).toBe(0);
    })

})

describe('Inventory - getNumberOfItems()', () => {
    test('should be 0 with no items', () => {
        const inventory: Inventory = new Inventory(25);
        expect(inventory.getNumberOfItems()).toBe(0)
    })

    test('should throw if no slots open', () => {
        const inventory: Inventory = new Inventory(0)
        expect(() => inventory.getFirstOpenSlot())
            .toThrow("Your inventory is full.");
    })
})

describe('Inventory - getNumberOfOpenSlots()', () => {

    test('No items in 25 slots should have 25 open', () => {
        const inventory: Inventory = new Inventory(25);
        expect(inventory.getNumberOfOpenSlots()).toBe(25);
    })

    test('10 slots with 2 items should be 8', () => {
        const inventory: Inventory = new Inventory(10);
        inventory.add(0, MOCK_CONSUMABLE_ITEMS[0]);
        inventory.add(1, MOCK_GEAR_ITEMS[0]);
        expect(inventory.getNumberOfOpenSlots()).toBe(8);
    })

})

describe('Inventory - add(slotIndex, item)', () => {

    test('should throw if no item given', () => {
        const inventory: Inventory = new Inventory(25);
        expect(() => inventory.add(0))
            .toThrow("No item given to add.")
    })

    test('should add 1 item to inventory', () => {
        const inventory: Inventory = new Inventory(25);
        inventory.add(0, MOCK_WEAPON_ITEMS[0]);
        expect(inventory.getNumberOfItems()).toBe(1);
    })

    test('should add 5 items to inventory', () => {
        const inventory: Inventory = new Inventory(25);

        inventory.add(0, MOCK_WEAPON_ITEMS[0])
        inventory.add(1, MOCK_WEAPON_ITEMS[1]);
        inventory.add(2, MOCK_CONSUMABLE_ITEMS[0]);
        inventory.add(3, MOCK_CONSUMABLE_ITEMS[1]);
        inventory.add(4, MOCK_GEAR_ITEMS[0]);

        expect(inventory.getNumberOfItems()).toBe(5);
    })

    // Needs refactoring. Test is actually checking getFirstOpenSlot().
    test('should throw if inventory is full', () => {
        const inventory: Inventory = new Inventory(1)
        inventory.add(0, MOCK_WEAPON_ITEMS[0]);
        expect(() => {
            const slotIndex = inventory.getFirstOpenSlot();
            inventory.add(slotIndex, MOCK_CONSUMABLE_ITEMS[0]);
        }).toThrow('Your inventory is full.');
    })

    test('should throw if no slotIndex', () => {
        const inventory: Inventory = new Inventory(2);
        expect(() => inventory.add(undefined, MOCK_WEAPON_ITEMS[0]))
            .toThrow("No slot provided.")
    })

    test('should throw when adding to slot 6 in 5 slots inventory', () => {
        const inventory: Inventory = new Inventory(5);
        expect(() => inventory.add(6, MOCK_GEAR_ITEMS[0]))
            .toThrow("You don't have that many slots available.");
    })

})

describe('Inventory - getItem(slotIndex)', () => {

    test('should get Wooden Sword from inventory', () => {
        const inventory: Inventory = new Inventory(5);
        inventory.add(0, new InventoryItem(
            "123233",
            "Wooden Sword",
            10,
            5,
            InventoryItemType.Weapon
        ));
        expect(inventory.getItem(0).getName()).toBe("Wooden Sword");
    })

    test('should throw if negative slotIndex given', () => {
        const inventory: Inventory = new Inventory(5);
        expect(() => inventory.getItem(-1)).toThrow("No negative slot indexes allowed.");
    })

    test('should throw if slotIndex not provided', () => {
        const inventory: Inventory = new Inventory(5);
        expect(() => inventory.getItem()).toThrow("No slot index provided.");
    })

    test('should not be undefined if item in slot', () => {
        const inventory: Inventory = new Inventory(5);
        inventory.add(0, MOCK_WEAPON_ITEMS[0]);
        expect(inventory.getItem(0)).not.toBe(undefined);
    })

    test('should throw nothing in slot', () => {
        const inventory: Inventory = new Inventory(5);
        expect(() => inventory.getItem(0))
            .toThrow("There's nothing in that slot.");
    })

    test('should throw given slot 6 in 5 slot inventory', () => {
        const inventory: Inventory = new Inventory(5);
        expect(() => inventory.getItem(6)).toThrow("Inventory slot does not exist.");
    })


})

describe('Inventory - drop(slotIndex)', () => {

    test('should throw if negative slotIndex given', () => {
        const inventory: Inventory = new Inventory(5);
        expect(() => inventory.drop(-1)).toThrow("Can't provide a negative slot index");
    })

    test('should throw if slot 6 of 5 given', () => {
        const inventory: Inventory = new Inventory(5);
        expect(() => inventory.drop(6)).toThrow("That slot does not exist.");
    })

    test('should throw if no slotIndex given', () => {
        const inventory: Inventory = new Inventory(5);
        expect(() => inventory.drop()).toThrow("No slot index given");
    })

    test('should remove Cracked Sword from inventory', () => {
        
        const crackedSword: InventoryItem = new InventoryItem(
            "123123", "Cracked Sword", 10, 10, InventoryItemType.Weapon
        );
        
        const swordIndex: number = 0;
        const inventory: Inventory = new Inventory(5);
        
        inventory.add(swordIndex, crackedSword);
        inventory.add(1, MOCK_GEAR_ITEMS[0]);

        inventory.drop(swordIndex);

        expect(() => inventory.getItem(swordIndex)).toThrow("There's nothing in that slot.");
    })

    test('should remove small potion from inventory', () => {

        const inventory: Inventory = new Inventory(5);
        inventory.add(2, new InventoryItem(
            "123123",
            "Small Potion",
            10, 
            15,
            InventoryItemType.Consumable
        ))

        inventory.drop(2);

        expect(() => inventory.getItem(2)).toThrow("There's nothing in that slot.");

    })
})

describe('Inventory - sellItem(index)', () => {
    test('should sell Dagger and add 10 to wallet', () => {
        const inventory: Inventory = new Inventory(5, new Wallet());
        inventory.add(0, new InventoryItem("123", "Dagger", 10, 10, InventoryItemType.Weapon));
        inventory.sellItem(0);
        expect(inventory.getWallet().getValue()).toBe(10);
    });

    test('should sell Steel Sword, add 20 to wallet of 30 to be 50', () => {
        const inventory: Inventory = new Inventory(5, new Wallet());
        inventory.getWallet().add(30);
        inventory.add(0, new InventoryItem("123", "Steel Sword", 50, 20, InventoryItemType.Weapon));
        inventory.sellItem(0);
        expect(inventory.getWallet().getValue()).toBe(50);
    })

    test('should sell Wooden Shield and remove from slot', () => {
        const inventory: Inventory = new Inventory(5);
        inventory.add(0, new InventoryItem("123", "Wooden Shield", 10, 10, InventoryItemType.Weapon));
        inventory.sellItem(0);
        expect(() => inventory.getItem(0)).toThrow("There's nothing in that slot.");
    })
})