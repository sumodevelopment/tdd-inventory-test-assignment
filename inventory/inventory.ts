import { InventoryItem } from "../inventory-item/inventory-item"
import Wallet from '../wallet/wallet';

class Inventory {

    private slots: InventoryItem[];
    private wallet: Wallet;

    constructor(inventorySize = 25, wallet: Wallet = new Wallet()) {
        this.slots = new Array(inventorySize).fill(undefined);
        this.wallet = wallet;
    }

    getWallet(): Wallet {
        return this.wallet;
    }

    getInventorySlotCount(): number {
        return this.slots.length;
    }

    getNumberOfItems(): Number {
        return this.slots.filter(slot => slot !== undefined).length;
    }

    getItem(slotIndex: number): InventoryItem {
        if (slotIndex === undefined) {
            throw new Error("No slot index provided.");
        }

        if (slotIndex < 0) {
            throw new Error("No negative slot indexes allowed.");
        }

        if (slotIndex > this.slots.length - 1) {
            throw new Error("Inventory slot does not exist.");
        }

        const item: InventoryItem = this.slots[slotIndex];

        if (item === undefined) {
            throw new Error("There's nothing in that slot.");
        }

        return item;
    }

    getNumberOfOpenSlots(): Number{
        return this.slots.filter(slot => slot === undefined).length;
    }

    getFirstOpenSlot(): number {

        const slot: number = this.slots.findIndex(slot => slot === undefined);
        
        if (slot === -1) {
            throw new Error('Your inventory is full.');
        }

        return slot;
    }

    add(slotIndex: number, item: InventoryItem): void {
        
        if (slotIndex === undefined) {
            throw new Error("No slot provided.")
        }

        if (item === undefined) {
            throw new Error("No item given to add.")
        }

        if (slotIndex > this.slots.length - 1) {
            throw new Error("You don't have that many slots available.");
        }
        
        this.slots[slotIndex] = item;
    }

    sellItem(slotIndex: number): void {
        this.wallet.add(this.slots[slotIndex].getValue());
        this.slots[slotIndex] = undefined;
    }

    drop(slotIndex: number): void {
        if (slotIndex === undefined) {
            throw new Error("No slot index given.");
        }
        if (slotIndex < 0) {
            throw new Error("Can't provide a negative slot index.");
        }
        if (slotIndex > this.slots.length - 1) {
            throw new Error("That slot does not exist.");
        }
        this.slots[slotIndex] = undefined;
    }

}

export default Inventory