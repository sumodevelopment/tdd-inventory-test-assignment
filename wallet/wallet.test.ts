import Wallet from "./wallet"

describe('Wallet - new Wallet()', () => {

    test('Create instance of wallet', () => {
        let wallet = new Wallet();
        expect(wallet).toBeInstanceOf(Wallet);
    });

    test('New instance should have value of 0', () => {
        let wallet = new Wallet();
        expect(wallet.getValue()).toBe(0);
    })

    test('New instance should not have value of 10', () => {
        const wallet = new Wallet();
        expect(wallet.getValue()).not.toBe(10);
    })

    test('Instantiate with 50 coins', () => {
        const wallet = new Wallet(50);
        expect(wallet.getValue()).toBe(50);
    })

    test('should throw if instance with negative -10 coins', () => {
        expect(() => new Wallet(-10)).toThrow("You can't create a wallet with negative coins.");
    })

    test('should throw if instance with "ten" coins', () => {
        expect(() => new Wallet("ten")).toThrow("You can't add words to a wallet.");
    })
})

describe('Wallet - add(coins)', () => {

    test('10 coins to empty wallet should be 10', () => {
        const wallet = new Wallet();
        expect(wallet.add(10)).toBe(10);
    })

    test('10 coins to 10 wallet should be 20', () => {
        const wallet = new Wallet(10);
        expect(wallet.add(10)).toBe(20);
    })

    test('should throw if 0 added', () => {
        const wallet = new Wallet();
        expect(() => wallet.add(0)).toThrow("You can't add no coins to a wallet.")
    })

    test('should throw if -10 added', () => {
        const wallet = new Wallet();
        expect(() => wallet.add(-10)).toThrow("You can't add no coins to a wallet.")
    })
})

describe('Wallet - use(coins)', () => {

    test('should throw if use 10 of 0 coins', () => {
        const wallet = new Wallet();
        expect(() => wallet.use(10)).toThrow("You don't have any coins to use.");
    })

    test('should throw if use 50 of 20 coins', () => {
        const wallet = new Wallet(20);
        expect(() => wallet.use(50)).toThrow("You don't have enough coins to use.");
    })

    test('should use 10 of 10 coins and be 0', () => {
        const wallet = new Wallet(10);
        expect(wallet.use(10)).toBe(0);
    })

    test('should use 20 of 100 coins and be 80', () => {
        const wallet = new Wallet(100);
        expect(wallet.use(20)).toBe(80);
    })

    test('should throw when using 10.5', () => {
        const wallet = new Wallet();
        expect(() => wallet.use(10.5)).toThrow("You can't break a coin.")
    })

    test('should throw using "ten"', () => {
        const wallet = new Wallet();
        expect(() => wallet.use("ten")).toThrow("You can only use coins, not words.");
    })

    test('should throw if no coins provided', () => {
        const wallet = new Wallet();
        expect(() => wallet.use()).toThrow("How many coins do you want to use?");
    })


})