class Wallet {

    private value: number;

    constructor(startWithCoins = 0) {

        if (typeof startWithCoins !== 'number') {
            throw new Error("You can't add words to a wallet.");
        }

        if (startWithCoins < 0){
            throw new Error("You can't create a wallet with negative coins.");
        }
        
        this.value = startWithCoins;
    }

    public add(coins: number): number {
        if (coins <= 0) {
            throw new Error("You can't add no coins to a wallet.");
        }
        this.value += coins;
        return this.value;
    }

    public use(coins: number): number {
        if (coins === undefined) {
            throw new Error("How many coins do you want to use?");
        }
        if (typeof coins !== 'number') {
            throw new Error("You can only use coins, not words.");
        }
        if (coins.toString().includes(".")) {
            throw new Error("You can't break a coin.");
        }

        if (this.value === 0){
            throw new Error("You don't have any coins to use.");
        }

        if (coins > this.value) {
            throw new Error("You don't have enough coins to use.");
        }

        this.value -= coins;
        return this.value;
    }

    public getValue(): number {
        return this.value;
    }
}

export default Wallet;